defmodule Core.MetroCdmx.DataLoad do
  @moduledoc """
  The following program consists of relating the lines of the city of Mexico
  with their respective stations, as well as obtaining a graph with the Graph function
  """

  use Core.DataLoad
  import SweetXml
  alias Core.Model.{Station, Line}

  def lines() do
    doc = File.read!(file()) |> SweetXml.parse()

    name_lines =
      doc
      |> xpath(~x"//Document/Folder[1]/Placemark/name/text()"l)
      |> Enum.map(fn l -> List.to_string(l) end)

    name_lines
    |> Enum.map(fn item ->
      station =
        doc
        |> xpath(
          ~x"//Document/Folder[1]/Placemark[name=\"#{item}\"]/LineString/coordinates/text()"l
        )
        |> Enum.map(fn l -> List.to_string(l) end)
        |> List.first()
        |> String.split()
        |> Enum.map(fn coor ->
          %Station{
            name:
              doc
              |> xpath(
                ~x"//Document/Folder[2]/Placemark[contains(./Point/coordinates,\"#{coor}\")]/name/text()"l
              )
              |> List.to_string(),
            coords: change_coordinates(coor)
          }
        end)

      %Line{
        name: item,
        stations: station
      }
    end)
    |> Enum.map(fn line ->
      %Line{
        name: line.name,
        stations: line.stations |> Enum.filter(fn station -> station.name != "" end)
      }
    end)
  end

  defp file() do
    if Mix.Project.umbrella?() do
      "apps/core/assets/Metro_CDMX.kml"
    else
      "../core/assets/Metro_CDMX.kml"
    end
  end
end

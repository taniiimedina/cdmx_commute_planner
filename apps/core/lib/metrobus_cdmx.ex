defmodule Core.MetrobusCdmx do
  @moduledoc """
  This module finds the shortest path between two CDMX metro stations.
  """

  use Core.PathFinder
  alias Core.MetrobusCdmx.DataManager

  def system_graph() do
    DataManager.graph()
  end

  def system_stations() do
    DataManager.stations()
  end
end

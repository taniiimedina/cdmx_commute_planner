defmodule Core.Model.Station do
  @moduledoc """
  Struct of the station
  """
  defstruct [:name, :coords]
end

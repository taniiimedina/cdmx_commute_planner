defmodule Core.Model.Segment do
  @moduledoc """
  Struct of the segment
  """
  # @file Application.compile_env!(:core, :file)
  @derive Jason.Encoder
  defstruct segment: 0, line: "", steps: ""

  @type t :: %__MODULE__{
          segment: integer(),
          line: String.t(),
          steps: String.t()
        }
end

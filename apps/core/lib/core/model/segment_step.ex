defmodule Core.Model.Segment.Step do
  @moduledoc """
  Struct of the Route
  """
  @derive Jason.Encoder
  defstruct [:index, :name, :coords]
end

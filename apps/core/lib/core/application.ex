defmodule Core.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {
        Core.MetroCdmx.DataManager,
        name: MetroCdmx.DataManager
      },
      {
        Core.MetrobusCdmx.DataManager,
        name: MetrobusCdmx.DataManager
      }
    ]

    opts = [strategy: :one_for_one, name: Core.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

defmodule Core.DataLoad do
  @moduledoc """
  This module defines functionality commmon to transport systems data load.
  """

  defmacro __using__(_opts) do
    quote do
      def database() do
        sys_lines = lines()
        sys_stations = stations(sys_lines)
        sys_graph = graph(sys_lines)
        {sys_stations, sys_graph}
      end

      defp stations(system_lines) do
        system_lines
        |> Enum.flat_map(fn line -> line.stations end)
        |> Enum.reduce(%{}, fn station, acc -> Map.put(acc, station.name, station) end)
      end

      defp graph(system_lines) do
        lines = system_lines
        graph = Graph.new(type: :directed)

        Enum.reduce(lines, graph, fn line, graph ->
          bi_station = Enum.chunk_every(line.stations, 2, 1, :discard)

          Enum.reduce(bi_station, graph, fn station, graph ->
            tmp_graph =
              Graph.add_edge(graph, List.first(station).name, List.last(station).name,
                label: line.name
              )

            Graph.add_edge(tmp_graph, List.last(station).name, List.first(station).name,
              label: line.name
            )
          end)
        end)
      end

      defp change_coordinates(coordinates) when is_binary(coordinates) do
        [second, first, _third] = String.split(coordinates, ",")
        "#{first},#{second}"
      end
    end
  end
end

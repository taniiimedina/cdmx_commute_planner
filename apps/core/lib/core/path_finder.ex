defmodule Core.PathFinder do
  @moduledoc """
  This moudule implements the generic code for finding paths within a transportation system.
  """

  defmacro __using__(_opts) do
    quote do
      alias Core.Model.Segment

      def find_path(origin, goal) do
        metro = system_graph()
        path = Graph.get_shortest_path(metro, origin, goal)

        if is_nil(path) do
          :error
        else
          path
          |> Enum.zip(Enum.drop(path, 1))
          |> Enum.map(fn {s, d} -> metro |> Graph.edges(s, d) |> hd() end)
          |> Stream.with_index()
          |> Enum.into([])
          |> Enum.map(fn {map, index} -> Map.put(map, :index, index + 1) end)
          |> Enum.group_by(fn e -> e.label end)
          |> Map.to_list()
          |> Enum.sort_by(fn {_k, v} -> v end)
          |> Stream.with_index()
          |> Enum.into([])
          |> Enum.map(fn {{line, tracks}, segment} ->
            stations = system_stations()

            route =
              Stream.with_index(tracks)
              |> Enum.into([])
              |> Enum.map(fn {track, index} ->
                Map.put(track, :index, index)

                %Segment.Step{
                  index: index + 1,
                  coords: stations[track |> Map.get(:v1)].coords,
                  name: track |> Map.get(:v1)
                }
              end)

            last = List.last(tracks) |> Map.get(:v2)

            new_route =
              route ++
                [
                  %Segment.Step{
                    index: Enum.count(route) + 1,
                    coords: stations[last].coords,
                    name: last
                  }
                ]

            %Segment{
              segment: segment + 1,
              line: line,
              steps: new_route
            }
          end)
        end
      end
    end
  end
end

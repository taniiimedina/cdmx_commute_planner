defmodule Core.MetrobusCdmx.DataManager do
  @moduledoc """
  This GensServer is intended to load the CDMX Metrobus graph at application startup and serve
  calls to retrieve such graph through the `metro_graph/0` public function.
  """

  alias Core.MetrobusCdmx.DataLoad
  require Logger
  use GenServer

  def start_link(name: name) do
    GenServer.start_link(__MODULE__, [], name: name)
  end

  @impl true
  def init(_) do
    Logger.info("Loading Metrobus CDMX database")

    {time_micro_secs, {stations, graph}} = :timer.tc(fn -> DataLoad.database() end)

    secs = Kernel./(time_micro_secs, 1_000_000)
    Logger.info("Loaded Metrobus CDMX database. #{secs} seconds")

    {:ok, {stations, graph}}
  end

  @impl true
  def handle_call(:graph, _from, {_stations, graph} = state) do
    {:reply, graph, state}
  end

  @impl true
  def handle_call(:stations, _from, {stations, _graph} = state) do
    {:reply, stations, state}
  end

  def graph() do
    GenServer.call(MetrobusCdmx.DataManager, :graph)
  end

  def stations() do
    GenServer.call(MetrobusCdmx.DataManager, :stations)
  end
end

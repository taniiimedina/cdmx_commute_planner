defmodule MetrobusCdmxTest do
  @moduledoc """
   module test metrobus_cdmx
  """
  use ExUnit.Case
  doctest Core.MetrobusCdmx

  describe "test Metrobus Cdmx" do
    test "validate the route with a segment" do
      assert Core.MetrobusCdmx.find_path("Necaxa", "Garibaldi") ==
               [
                 %Core.Model.Segment{
                   line: "Linea 07",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.47537576199611, -99.12231816907183",
                       index: 1,
                       name: "Necaxa"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.47119438241247, -99.12388600766809",
                       index: 2,
                       name: "Excélsior"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.46775822598716, -99.12517766414921",
                       index: 3,
                       name: "Robles Domínguez"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.46464668699436, -99.12634717960675",
                       index: 4,
                       name: "Clave"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4617545271578, -99.12743806383541",
                       index: 5,
                       name: "Misterios"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.45732546502067, -99.12912522231824",
                       index: 6,
                       name: "Mercado Beethoven"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4531189777083, -99.1308624235438",
                       index: 7,
                       name: "Peralvillo"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.45060233813405, -99.13282697755517",
                       index: 8,
                       name: "Tres Culturas"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.44737666688031, -99.13563175311776",
                       index: 9,
                       name: "Glorieta Cuitláhuac"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.44439129247632, -99.1389717441165",
                       index: 10,
                       name: "Garibaldi"
                     }
                   ]
                 }
               ]
    end

    test "validate the route with three segment" do
      assert Core.MetrobusCdmx.find_path("Gran Canal", "Morelos") ==
               [
                 %Core.Model.Segment{
                   line: "Linea 06",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.47709475661586, -99.09293319630496",
                       index: 1,
                       name: "Gran Canal"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.47875943591403, -99.09717411378594",
                       index: 2,
                       name: "San Juan de Aragón"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Linea 05",
                   segment: 2,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.47875943591403, -99.09717411378594",
                       index: 1,
                       name: "San Juan de Aragón"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.47602758536352, -99.09911652230174",
                       index: 2,
                       name: "Río de Guadalupe"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.47101723626883, -99.10124294394696",
                       index: 3,
                       name: "Talismán"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.46734152454793, -99.10279733696791",
                       index: 4,
                       name: "Victoria"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.46097481439908, -99.10548605263332",
                       index: 5,
                       name: "Oriente 101"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.45605858106011, -99.10749291622152",
                       index: 6,
                       name: "Río Santa Coleta"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.45366484682488, -99.10844651603701",
                       index: 7,
                       name: "Río Consulado"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.44968638877538, -99.11035715164073",
                       index: 8,
                       name: "Canal del Norte"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.44469916077692, -99.11217545330651",
                       index: 9,
                       name: "Deportivo Eduardo Molina"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.44132427865846, -99.11343961635318",
                       index: 10,
                       name: "Mercado Morelos"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4371747005664, -99.11439931807691",
                       index: 11,
                       name: "Archivo General de la Nación"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Linea 04",
                   segment: 3,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4371747005664, -99.11439931807691",
                       index: 1,
                       name: "Archivo General de la Nación"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.43583653683108, -99.11875199998471",
                       index: 2,
                       name: "Morelos"
                     }
                   ]
                 }
               ]
    end

    test "error when entering wrong name for the origin" do
      assert Core.MetrobusCdmx.find_path("Grancanal", "Morelos") == :error
    end

    test "error when entering wrong the name of the destination" do
      assert Core.MetrobusCdmx.find_path("Gran Canal", "Morelia") == :error
    end
  end
end

defmodule MetroCdmxTest do
  use ExUnit.Case
  doctest Core.MetroCdmx

  describe "test MetroCdmx" do
    test "validate the route with a segment" do
      assert Core.MetroCdmx.find_path("Observatorio", "Insurgentes") ==
               [
                 %Core.Model.Segment{
                   line: "Línea 1",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.3982501,-99.2005488",
                       index: 1,
                       name: "Observatorio"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4031605,-99.187097",
                       index: 2,
                       name: "Tacubaya"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4129053,-99.1821724",
                       index: 3,
                       name: "Juanacatlán"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.420813,-99.1762608",
                       index: 4,
                       name: "Chapultepec"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.421926,-99.1706067",
                       index: 5,
                       name: "Sevilla"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4236259,-99.1627908",
                       index: 6,
                       name: "Insurgentes"
                     }
                   ]
                 }
               ]
    end

    test "validate the route with two segments" do
      assert Core.MetroCdmx.find_path("Observatorio", "Chabacano") ==
               [
                 %Core.Model.Segment{
                   line: "Línea 1",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.3982501,-99.2005488",
                       index: 1,
                       name: "Observatorio"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4031605,-99.187097",
                       index: 2,
                       name: "Tacubaya"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 9",
                   segment: 2,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4031605,-99.187097",
                       index: 1,
                       name: "Tacubaya"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4062317,-99.1788948",
                       index: 2,
                       name: "Patriotismo"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.406171,-99.1684502",
                       index: 3,
                       name: "Chilpancingo"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4066618,-99.1552055",
                       index: 4,
                       name: "Centro Médico"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.407021,-99.1448736",
                       index: 5,
                       name: "Lázaro Cárdenas"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4084883,-99.1357434",
                       index: 6,
                       name: "Chabacano"
                     }
                   ]
                 }
               ]
    end

    test "validate the route with three segments" do
      assert Core.MetroCdmx.find_path("Normal", "Morelos") ==
               [
                 %Core.Model.Segment{
                   line: "Línea 2",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4447058,-99.1673774",
                       index: 1,
                       name: "Normal"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.441782,-99.1611063",
                       index: 2,
                       name: "San Cosme"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4397232,-99.1555005",
                       index: 3,
                       name: "Revolución"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4373152,-99.1471374",
                       index: 4,
                       name: "Hidalgo"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4361922,-99.1419393",
                       index: 5,
                       name: "Bellas Artes"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 8",
                   segment: 2,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4361922,-99.1419393",
                       index: 1,
                       name: "Bellas Artes"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4427583,-99.1392624",
                       index: 2,
                       name: "Garibaldi"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea B",
                   segment: 3,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4427583,-99.1392624",
                       index: 1,
                       name: "Garibaldi"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4433754,-99.1313499",
                       index: 2,
                       name: "Lagunilla"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4425205,-99.1233194",
                       index: 3,
                       name: "Tepito"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4389745,-99.1182554",
                       index: 4,
                       name: "Morelos"
                     }
                   ]
                 }
               ]
    end

    test "validate the route with four segments" do
      assert Core.MetroCdmx.find_path("Ermita", "Misterios") ==
               [
                 %Core.Model.Segment{
                   line: "Línea 2",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.3619032,-99.1429317",
                       index: 1,
                       name: "Ermita"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.369945,-99.1415638",
                       index: 2,
                       name: "Portales"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.3795146127671,-99.140260219574",
                       index: 3,
                       name: "Nativitas"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.3874695,-99.1390371",
                       index: 4,
                       name: "Villa de Cortés"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.3952064,-99.1378087",
                       index: 5,
                       name: "Xola"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4008785,-99.1368914",
                       index: 6,
                       name: "Viaducto"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4084883,-99.1357434",
                       index: 7,
                       name: "Chabacano"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 9",
                   segment: 2,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4084883,-99.1357434",
                       index: 1,
                       name: "Chabacano"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4091258,-99.1217637",
                       index: 2,
                       name: "Jamaica"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 4",
                   segment: 3,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4091258,-99.1217637",
                       index: 1,
                       name: "Jamaica"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4216528,-99.1205406",
                       index: 2,
                       name: "Fray Servando"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4288468,-99.1194355",
                       index: 3,
                       name: "Candelaria"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4389745,-99.1182554",
                       index: 4,
                       name: "Morelos"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4488638,-99.1161579",
                       index: 5,
                       name: "Canal del Norte"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4580545,-99.1139048",
                       index: 6,
                       name: "Consulado"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 5",
                   segment: 4,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4580545,-99.1139048",
                       index: 1,
                       name: "Consulado"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.458798,-99.1193229",
                       index: 2,
                       name: "Valle Gómez"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4634058,-99.1307974",
                       index: 3,
                       name: "Misterios"
                     }
                   ]
                 }
               ]
    end

    test "validate the route with five segments" do
      assert Core.MetroCdmx.find_path("Indios Verdes", "Puebla") ==
               [
                 %Core.Model.Segment{
                   line: "Línea 3",
                   segment: 1,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4953987,-99.1194999",
                       index: 1,
                       name: "Indios Verdes"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4837877,-99.1265702",
                       index: 2,
                       name: "Deportivo 18 de Marzo"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 6",
                   segment: 2,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4837877,-99.1265702",
                       index: 1,
                       name: "Deportivo 18 de Marzo"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4815524,-99.1179228",
                       index: 2,
                       name: "La Villa | Basílica de Guadalupe"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.482574,-99.1070652",
                       index: 3,
                       name: "Martin Carrera"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 4",
                   segment: 3,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.482574,-99.1070652",
                       index: 1,
                       name: "Martin Carrera"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4743304,-99.1080093",
                       index: 2,
                       name: "Talismán"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4647057,-99.1119307",
                       index: 3,
                       name: "Bondojito"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4580545,-99.1139048",
                       index: 4,
                       name: "Consulado"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 5",
                   segment: 4,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4580545,-99.1139048",
                       index: 1,
                       name: "Consulado"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4514081,-99.105413",
                       index: 2,
                       name: "Eduardo Molina"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4511957,-99.0963578",
                       index: 3,
                       name: "Aragón"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4457529,-99.0871739",
                       index: 4,
                       name: "Oceanía"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4338247,-99.0876889",
                       index: 5,
                       name: "Terminal Aérea"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4240913,-99.0874207",
                       index: 6,
                       name: "Hangares"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4153591,-99.0722072",
                       index: 7,
                       name: "Pantitlán"
                     }
                   ]
                 },
                 %Core.Model.Segment{
                   line: "Línea 9",
                   segment: 5,
                   steps: [
                     %Core.Model.Segment.Step{
                       coords: "19.4153591,-99.0722072",
                       index: 1,
                       name: "Pantitlán"
                     },
                     %Core.Model.Segment.Step{
                       coords: "19.4072234,-99.0824747",
                       index: 2,
                       name: "Puebla"
                     }
                   ]
                 }
               ]
    end

    test "error when entering wrong name for the origin" do
      assert Core.MetroCdmx.find_path("Indios Verde", "Puebla") == :error
    end

    test "error when entering wrong the name of the destination" do
      assert Core.MetroCdmx.find_path("Indios Verdes", "puebla") == :error
    end
  end
end

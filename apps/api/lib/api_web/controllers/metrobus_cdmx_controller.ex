defmodule ApiWeb.MetrobusCdmxController do
  use ApiWeb, :controller
  alias Core.MetrobusCdmx

  def find_path(c, %{"origin" => origin, "dest" => dest}) do
    MetrobusCdmx.find_path(origin, dest)
    |> case do
      :error ->
        c
        |> put_status(404)
        |> json(%{error: "This path is wrong"})

      route ->
        render(c, "routes.json", origin: origin, dest: dest, route: route)
    end
  end

  def list_stations(c, _params) do
    stations = MetrobusCdmx.system_stations() |> Map.keys()
    render(c, "list.json", stations: stations)
  end
end

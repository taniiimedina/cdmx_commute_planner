defmodule ApiWeb.MetroCdmxController do
  use ApiWeb, :controller

  alias Core.MetroCdmx

  def find_path(c, %{"origin" => origin, "dest" => dest}) do
    MetroCdmx.find_path(origin, dest)
    |> case do
      :error ->
        c
        |> put_status(404)
        |> json(%{error: "This path is wrong"})

      route ->
        render(c, "routes.json", origin: origin, dest: dest, route: route)
    end
  end

  def list_stations(c, _params) do
    stations = MetroCdmx.system_stations() |> Map.keys()
    render(c, "list.json", stations: stations)
  end
end

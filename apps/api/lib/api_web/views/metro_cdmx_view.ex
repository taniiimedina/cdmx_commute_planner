defmodule ApiWeb.MetroCdmxView do
  use ApiWeb, :view

  def render("routes.json", data) do
    %{status: 200, origin: data.origin, dest: data.dest, itinerary: data.route}
  end

  def render("list.json", data) do
    %{status: 200, stations: data.stations}
  end
end

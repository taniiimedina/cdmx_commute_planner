defmodule ApiWeb.MetroCdmxControllerTest do
  use ApiWeb.ConnCase

  describe "test that the api Metro_Cdmx works by entering the data correctly and incorrectly" do
    test "test succesful", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.metro_cdmx_path(conn, :find_path, %{origin: "Observatorio", dest: "Chabacano"})
        )

      assert json_response(conn, 200) == %{
               "dest" => "Chabacano",
               "itinerary" => [
                 %{
                   "line" => "Línea 1",
                   "segment" => 1,
                   "steps" => [
                     %{
                       "coords" => "19.3982501,-99.2005488",
                       "index" => 1,
                       "name" => "Observatorio"
                     },
                     %{"coords" => "19.4031605,-99.187097", "index" => 2, "name" => "Tacubaya"}
                   ]
                 },
                 %{
                   "line" => "Línea 9",
                   "segment" => 2,
                   "steps" => [
                     %{"coords" => "19.4031605,-99.187097", "index" => 1, "name" => "Tacubaya"},
                     %{
                       "coords" => "19.4062317,-99.1788948",
                       "index" => 2,
                       "name" => "Patriotismo"
                     },
                     %{
                       "coords" => "19.406171,-99.1684502",
                       "index" => 3,
                       "name" => "Chilpancingo"
                     },
                     %{
                       "coords" => "19.4066618,-99.1552055",
                       "index" => 4,
                       "name" => "Centro Médico"
                     },
                     %{
                       "coords" => "19.407021,-99.1448736",
                       "index" => 5,
                       "name" => "Lázaro Cárdenas"
                     },
                     %{"coords" => "19.4084883,-99.1357434", "index" => 6, "name" => "Chabacano"}
                   ]
                 }
               ],
               "origin" => "Observatorio",
               "status" => 200
             }
    end

    test "test fail", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.metro_cdmx_path(conn, :find_path, %{origin: "Observatorio", dest: "Chaba"})
        )

      assert json_response(conn, 404) == %{
               "error" => "This path is wrong"
             }
    end
  end
end

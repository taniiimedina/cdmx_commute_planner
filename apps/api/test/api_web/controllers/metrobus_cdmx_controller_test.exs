defmodule ApiWeb.MetrobusCdmxControllerTest do
  use ApiWeb.ConnCase

  describe "test that the api Metrobus_Cdmx works by entering the data correctly and incorrectly" do
    test "test succesful", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.metrobus_cdmx_path(conn, :find_path, %{origin: "Clave", dest: "Necaxa"})
        )

      assert json_response(conn, 200) == %{
               "dest" => "Necaxa",
               "itinerary" => [
                 %{
                   "line" => "Linea 07",
                   "segment" => 1,
                   "steps" => [
                     %{
                       "coords" => "19.46464668699436, -99.12634717960675",
                       "index" => 1,
                       "name" => "Clave"
                     },
                     %{
                       "coords" => "19.46775822598716, -99.12517766414921",
                       "index" => 2,
                       "name" => "Robles Domínguez"
                     },
                     %{
                       "coords" => "19.47119438241247, -99.12388600766809",
                       "index" => 3,
                       "name" => "Excélsior"
                     },
                     %{
                       "coords" => "19.47537576199611, -99.12231816907183",
                       "index" => 4,
                       "name" => "Necaxa"
                     }
                   ]
                 }
               ],
               "origin" => "Clave",
               "status" => 200
             }
    end

    test "test fail", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.metrobus_cdmx_path(conn, :find_path, %{origin: "Clave", dest: "Necatsa"})
        )

      assert json_response(conn, 404) == %{
               "error" => "This path is wrong"
             }
    end
  end
end

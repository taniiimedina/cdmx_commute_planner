# Load Test Code

## Requirements

In orther to run the load tests, you must install

1. [Tsung 1.7.0](http://tsung.erlang-projects.org/). Installation instructions depend on your operating system.
2. Pearl Template module (used for report generation). To install Template issue the following command:

```bash
$ cpan install Template
```

## Execution

To run a load test, adjust the params `duration` and `arrivalrate` in `load.xml`.

```xml
    <arrivalphase phase="1" duration="2" unit="minute">
      <users arrivalrate="5" unit="second"/>
    </arrivalphase>
```

Then execute:

```bash
$ tsung -f load.xml start
Starting Tsung
Log directory is: /home/tania/.tsung/log/20220619-1432
[os_mon] memory supervisor port (memsup): Erlang has closed
[os_mon] cpu supervisor port (cpu_sup): Erlang has closed
```
## Report creation

Move to the directory created during the test

```bash
cd /home/tania/.tsung/log/20220619-1432
```

and run the report script

```bash
$ /usr/lib/tsung/bin/tsung_stats.pl
```

The you can open the generated report `report.html`.
